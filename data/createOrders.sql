CREATE TABLE ORDERS ( 
    ID            INTEGER PRIMARY KEY AUTOINCREMENT
                          NOT NULL,
    SOURCEID      INTEGER NOT NULL
                          REFERENCES POSITION ( ID ),
    DESTINATIONID INTEGER NOT NULL
                          REFERENCES POSITION ( ID ) 
);


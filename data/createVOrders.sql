CREATE VIEW vOrders AS
       SELECT Orders.ID AS Id,
              P1.Description AS Source,
              P2.Description AS Destination,
              ( 
                  SELECT Time
                    FROM Updated
                   WHERE OrderID = Orders.Id 
                         AND
                         ID = ( 
                             SELECT Max( ID )
                              WHERE OrderID = Orders.Id 
                         ) 
                          
              ) 
              AS LastUpdate,
              ( 
                  SELECT Status.Description
                    FROM Updated
                         INNER JOIN Status
                                 ON Updated.StatusID = Status.Id
                   WHERE Updated.ID = ( 
                             SELECT Max( Updated.ID )
                               FROM Updated
                              WHERE OrderID = Orders.Id 
                         ) 
                          
              ) 
              AS Status
         FROM Orders
              INNER JOIN Position AS P1
                      ON Orders.SourceID = P1.Id
              INNER JOIN Position AS P2
                      ON Orders.DestinationID = P2.Id
              INNER JOIN ExecutionOrder AS EO
                      ON EO.OrderID = Orders.ID
        ORDER BY EO.ID;

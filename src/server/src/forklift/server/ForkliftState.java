package forklift.server;

/**
 * Die verschiedenen Zustände in denen sich der Gabelstapler befinden kann
 *
 * @author David Klein
 */
public enum ForkliftState {
    IDLE, // Forklift wartet auf einen Auftrag
    PROCESSING_ORDER // Forklift arbeitet aktuell einen Auftrag ab

}

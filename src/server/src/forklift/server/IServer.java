package forklift.server;

import forklift.common.Order;
import forklift.common.Position;
import forklift.common.Status;
import forklift.server.Repository.IRepository;
import forklift.server.nxt.NXTInfo;

import java.util.Iterator;
import java.util.Set;

/**
 * Schnittstelle für den Server, verschiedene Implementationen könnten diese beispielsweise via Webservices über das Netzwerk bereitstellen.
 *
 * @author David Klein
 */
public interface IServer {
    /**
     * Reserviert eine ID für einen Auftrag in der Staging Area des Clients
     * @return ID
     */
    public int getOrderId();

    /**
     * Übermittelt eine Menge von Aufträgen zur abarbeitung an den Server
     * @param orders Menge von Aufträgen
     */
    public void queueOrders(Iterator<Order> orders);

    /**
     * Gibt die aktuelle Position des Gabelstaplers zurück
     * @return Die aktuelle Position
     */
    public Position getForkliftPosition();

    /**
     * Gibt alle gespeicherten Aufträge zurück
     * @return Alle Aufträge aus der Datenbank
     */
    public Iterator<Order> getOrders();

    /**
     * Gibt alle gespeicherten Aufträge mit Status == s zurück
     * @param s s.o.
     * @return s.o.
     */
    public Iterator<Order> getOrdersByStatus(Status s);

    /**
     * Sucht nach dem Auftrag mit der Id id
     * @param id s.o.
     * @return Der Auftrag oder null falls es keinen Auftrag mit dieser ID gibt
     */
    public Order getOrderById(int id);

    /**
     * Die aktuelle Konfiguration der Paletten auf dem Feld
     * @return s.o.
     */
    public Iterator<Position> getCargoPositions();

    /**
     * Setzt das Objekt, welches für die Verwaltung/Speicherung der Aufträge verantwortlich ist
     * @param repository s.o.
     */
    public void setRepository(IRepository repository);

    /**
     * Startet den Server
     */
    public void run();

    /**
     * Beendet den Server
     */
    public void shutdown();

    /**
     * Setzt die Initialkonfiguration der Paletten
     * @param positions Die Initialkonfiguration
     */
    public void setDefaultCargoPositions(Set<Position> positions);

    /**
     * Gibt die aktuellen Statusinformationen zum NXT zurück
     * @return Aktuelle Statusinformationen oder null wenn noch keine verfügbar sind
     */
    public NXTInfo getNXTInfo();
}

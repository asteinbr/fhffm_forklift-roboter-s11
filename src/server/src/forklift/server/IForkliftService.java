package forklift.server;

import forklift.common.Order;
import forklift.common.Position;
import forklift.server.nxt.NXTInfo;

/**
 * Der Server erzeugt ein Objekt das dieses Interface implementiert, damit der Forklift Nachrichten an den Server zurückgeben kann
 *
 * @author David Klein
 */
public interface IForkliftService {
    /**
     * Liest den nächsten Auftrag aus dem Repository aus
     * @return Der nächste abzuarbeitende Auftrag
     */
    public Order getNextOrder();

    /**
     * Setzt die aktuelle Position des Gabelstaplers
     * @param p Die aktuelle Position
     */
    public void setCurrentPosition(Position p);

    /**
     * Setzt die Statusinformationen vom NXT
     * @param nxtInfo Die aktuellen Statusinformationen
     */
    public void setNXTInfo(NXTInfo nxtInfo);

    /**
     * Der Forklift hat den Auftrag abgeschlossen
     */
    public void orderCompleted();
}

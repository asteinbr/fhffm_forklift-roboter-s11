package forklift.server;

import forklift.common.*;
import forklift.server.Repository.IRepository;
import forklift.server.nxt.NXTInfo;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implementation von IServer, welche dessen Funktionalität für den direkten Zugriff bereitstellt.
 *
 * @author David Klein
 */
public class Server implements IServer {
    private IRepository repository = null;
    private Position currentPosition = null;
    private Order currentOrder = null;
    private final Set<Position> currentPositions;
    private volatile NXTInfo nxtInfo = null;
    private final static Logger LOGGER = Logger.getLogger(Server.class.getName());

    public Server() {
        this.currentPositions = new HashSet<Position>();
    }

    public void setCurrentPosition(String position) {
        this.currentPosition = Position.valueOf(position);
    }

    public void setRepository(IRepository repo) {
        if(repository == null) {
            repository = repo;
        } else {
            LOGGER.severe("Repository bereits initialisiert");
            throw new IllegalStateException("Repository bereits initialisiert");
        }
    }

    @Override
    public int getOrderId() {
        return repository.getNextId();
    }

    @Override
    public void queueOrders(Iterator<Order> orders) {
        while(orders.hasNext()) {
            repository.persist(orders.next());
        }
    }

    @Override
    public Position getForkliftPosition() {
        return currentPosition;
    }
    @Override
    public Iterator<Order> getOrders() {
        return repository.getOrders();
    }

    @Override
    public Iterator<Order> getOrdersByStatus(Status s) {
        return repository.getOrdersByStatus(s);
    }

    @Override
    public Order getOrderById(int id) {
        return repository.getOrderById(id);
    }

    @Override
    public Iterator<Position> getCargoPositions() {
        return currentPositions.iterator();
    }

    @Override
    public void run() {
        if(repository == null) {
            LOGGER.severe("Repository nicht initialisiert");
            throw new IllegalStateException("Repository nicht initialisiert");
        }
        if(currentPosition == null) {
            LOGGER.severe("Startposition nicht initialisiert");
            throw new IllegalStateException("Startposition nicht initialisiert");
        }
    }

    @Override
    public void shutdown() {
        repository.shutdown();
    }

    @Override
    public void setDefaultCargoPositions(Set<Position> positions) {
        this.currentPositions.addAll(positions);
    }

    @Override
    public synchronized NXTInfo getNXTInfo() {
        return nxtInfo;
    }

    public IForkliftService getForkliftService() {
        return new IForkliftService() {
            @Override
            public synchronized Order getNextOrder() {
                Iterator<Order> orders = repository.getOrdersByStatus(Status.OPEN);
                if(orders.hasNext()) {
                    Order o = orders.next();
                    repository.updateStatus(o,Status.ACCEPT);
                    currentOrder = o;
                    //System.err.println("getNextOrder " + o.toBluetoothString());
                    return o;
                }
                else {
                    return null;
                }
            }

            @Override
            public synchronized void setCurrentPosition(Position p) {
                currentPosition = p;
            }

            @Override
            public synchronized void setNXTInfo(NXTInfo info) {
                nxtInfo = info;
            }

            @Override
            public synchronized void orderCompleted() {
                currentPositions.remove(currentOrder.getSource());
                currentPositions.add(currentOrder.getDestination());
                repository.updateStatus(currentOrder, Status.DONE);
            }
        };
    }

}

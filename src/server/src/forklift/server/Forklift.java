package forklift.server;

import forklift.common.Order;
import forklift.common.Position;
import forklift.server.nxt.INXT;
import forklift.server.nxt.NXTInfo;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;


/**
 * Klasse die mit dem Gabelstapler kommuniziert und Informationen an den Server zurück gibt (via ForkliftService)
 *
 * @author David Klein
 */
public class Forklift {
    private Server server = null;
    private INXT nxt = null;
    private IForkliftService service = null;
    private ForkliftState state = ForkliftState.IDLE;
    //Solange running auf true ist, laufen die Hintergrundthreads
    private volatile boolean running = true;
    private final static Logger LOGGER = Logger.getLogger(Forklift.class.getName());
    private ExecutorService executor = null;

    /**
     * Thread der regelmäßig die aktuelle Position vom NXT abfragt
     */
    private final Runnable positionQuery = new Runnable() {
        @Override
        public void run() {

            while (running) {
                try {
                    synchronized (this) {
                        LOGGER.info("Positionsanfrage - enter");
                        String resp = nxt.receiveMessage(6);
                        if (!resp.equals("")) {
                            Position p = Position.valueOf(resp);
                            service.setCurrentPosition(p);
                            LOGGER.info("Positionsanfrage - result: " + p.toString());

                        } else {
                            LOGGER.info("Positionsanfrage - no result");
                        }
                        Thread.sleep(2000);

                    }

                } catch (IOException ex) {
                    System.err.println(ex.getMessage());
                } catch (InterruptedException ex) {
                    //nothing to do!
                }
            }
            LOGGER.info("positionQuery - exit");
        }
    };

    /**
     * Thread der Aufträge an den NXT schickt und die Quittierung für abgearbeitete Aufträge entgegennimmt
     */
    private final Runnable communicator = new Runnable() {
        @Override
        public void run() {
            while (running) {
                synchronized (this) {
                switch (state) {
                    case IDLE:

                            LOGGER.info("Idle - enter");
                            Order o = service.getNextOrder();
                            if (o == null) {
                                LOGGER.info("Idle - no new order");
                                try {
                                    /*synchronized (this) {
                                        this.wait(1000);
                                    }                    */
                                    Thread.sleep(1000);
                                } catch (InterruptedException ex) {
                                    //nothing to do
                                }

                            } else {
                                try {
                                    nxt.sendMessage(0, o.getSource().toString());
                                    nxt.sendMessage(1, o.getDestination().toString());
                                    nxt.sendMessage(2, "Job sent");
                                    state = ForkliftState.PROCESSING_ORDER;
                                    LOGGER.info("Idle - sent order");
                                } catch (IOException ex) {
                                    System.err.println(ex.getMessage());
                                }
                            }


                        break;
                    case PROCESSING_ORDER:
                        synchronized (this) {
                            try {
                                LOGGER.info("Processing - enter");
                                String resp = nxt.receiveMessage(5);
                                if (resp.equals("")) {
                                    LOGGER.info("Processing - still working");
                                    try {
                                        Thread.sleep(2000);
                                        /*synchronized (this) {
                                            this.wait(2000);
                                        }*/
                                    } catch (InterruptedException ex) {
                                        //nothing to do
                                    }
                                } else {
                                    LOGGER.info("Processing - finished");
                                    service.orderCompleted();
                                    state = ForkliftState.IDLE;
                                }
                            } catch (IOException ex) {
                                LOGGER.severe(ex.getMessage());
                                //throw new RuntimeException(ex);
                            }
                        }
                        break;

                    default:
                        throw new RuntimeException("unknown state -> should not happen");
                }
                }
            }
            LOGGER.info("communicator - exit");
        }
    };

    /**
     * Thread der regelmäßig die Statusinformationen vom NXT ausliest
     */
    private final Runnable deviceInfoQuery = new Runnable() {
        @Override
        public void run() {
            while (running) {
                LOGGER.info("InfoQuery - enter");
                try {
                    synchronized (this) {
                        NXTInfo info = nxt.readNXTInfo();
                        if(info == null) {
                            System.out.println("hmm");
                        }
                        service.setNXTInfo(info);
                    }


                } catch (IOException ex) {
                    LOGGER.severe(ex.getMessage());
                    //throw new RuntimeException(ex);
                }
                try {
                    /*synchronized (this) {
                        this.wait(5000);
                    }*/
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    //nothing to do
                }
                LOGGER.info("InfoQuery - completed");

            }
            LOGGER.info("deviceInfoQuery - exit");
        }
    };

    public void setServer(Server server) {
        this.server = server;
    }

    public void setNxt(INXT nxt) {
        this.nxt = nxt;
    }

    /**
     * Terminates the background Threads
     */
    public void shutdown() {
        LOGGER.info("Forklift - shutdown");
        this.running = false;
        executor.shutdown();
    }

    /**
     * Startet die Threads zur Kommunikation mit dem NXT
     */
    public void start()  {
        if (this.server == null) {
            LOGGER.severe("Server muss gesetzt sein");
            throw new IllegalStateException("Server muss gesetzt sein");
        }
        if (this.nxt == null) {
            LOGGER.severe("NXT muss gesetzt sein");
            throw new IllegalStateException("NXT muss gesetzt sein");
        }
        this.service = server.getForkliftService();

        executor = Executors.newFixedThreadPool(10);

        executor.execute(deviceInfoQuery);
        executor.execute(communicator);
        executor.execute(positionQuery);

    }

}

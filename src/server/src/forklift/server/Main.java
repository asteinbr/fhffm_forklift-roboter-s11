package forklift.server;

import forklift.common.*;
import forklift.server.Configuration.XMLConfigurationBasedFactory;
import forklift.server.Repository.IRepository;
import forklift.server.nxt.INXT;

import java.util.ArrayList;
import java.util.List;

/**
 * Mainklasse um die Serverfunktionalität auch ohne GUI zu testen
 *
 * @author David Klein
 */
public class Main {
    public static void main(String[] args) throws Exception {
        MyLogger.setup();
        XMLConfigurationBasedFactory c = new XMLConfigurationBasedFactory("src/server/config.xml");
        IRepository rep = c.getRepository();
        IServer server = c.getServer();
        INXT nxt = c.getNXT();
        server.setRepository(rep);
        server.run();
        Forklift forklift = new Forklift();
        forklift.setNxt(nxt);
        forklift.setServer((Server) server);
        //forklift.start();
        Thread.sleep(2000);
        List<Order> orders = new ArrayList<Order>();
        int id = server.getOrderId();
        Order o = new Order(id, Position.A1, Position.H8);
        orders.add(o);
        id = server.getOrderId();
        o = new Order(id, Position.A4, Position.H3);
        orders.add(o);
        id = server.getOrderId();
        o = new Order(id, Position.C6, Position.D5);
        orders.add(o);
        server.queueOrders(orders.iterator());

    }
}

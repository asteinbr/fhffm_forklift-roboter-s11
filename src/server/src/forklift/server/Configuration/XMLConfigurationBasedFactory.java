package forklift.server.Configuration;

import forklift.server.Repository.IRepository;
import forklift.server.IServer;
import forklift.server.Repository.DefaultCargoPositions;
import forklift.server.nxt.INXT;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Hashtable;
import java.util.Map;

/**
 * Eine Factory die auf Basis einer .xml Datei verschiedene Objekte erzeugt und deren Attribute dynamisch via Reflection setzt.
 *
 * @author David Klein
 */
public class XMLConfigurationBasedFactory {
    private Document doc = null;
    private XPath xpath = null;

    /**
     * Öffnet die zugehörige Konfigurationsdatei und bereitet sie für die XPath Nutzung vor
     *
     * @param filename Dateiname der Konfigurationsdatei
     */
    public XMLConfigurationBasedFactory(String filename) {
        try {
            DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = domFactory.newDocumentBuilder();
            doc = builder.parse(filename);
            xpath = XPathFactory.newInstance().newXPath();

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Erzeugt auf Basis der Konfigurationsdatei einen Container für die Ursprungskonfiguration der Paletten
     * @return Die Ursprungskonfiguration der Paletten
     */
    public DefaultCargoPositions getCargoPositions() {
        try {
            return (DefaultCargoPositions) getObj("DefaultCargoPositions");
        } catch(Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Erzeugt auf Basis der Konfigurationsdatei das Objekt welches die Kommunikation mit dem NXT vornimmt
     * @return Proxy für die NXT-Bluetooth Kommunikation
     */
    public INXT getNXT() {
        try {
            INXT nxt = (INXT) getObj("NXT");
            nxt.establishConnection();
            return nxt;
        } catch(Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Erzeugt auf Basis der geladenen Konfigurationsdatei eine Instanz des dort für 'Server' hinterlegten Klassennamens und setzt
     * die zugehörigen Attribute
     *
     * @return Die Instanz als IServer
     */
    public IServer getServer() {
        try {
            return (IServer) getObj("Server");
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Erzeugt auf Basis der geladenen Konfigurationsdatei eine Instanz des dort für 'Repository' hinterlegten Klassennamens und setzt
     * die zugehörigen Attribute
     *
     * @return Die Instanz als IRepository
     */
    public IRepository getRepository() {
        try {
            IRepository repository = (IRepository) getObj("Repository");
            repository.initialize();
            return repository;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Erzeugt eine neue Instanz der zu 'identifier' zugeordneten Klasse und setzt ihre Attribute anhand der
     * Konfigurationsdatei
     *
     * @param identifier Der Bezeichner der zu erzeugenden Klasse
     * @return Das erzeugte Objekt
     * @throws ClassNotFoundException Wenn das Instanziieren der Klasse fehltschlägt. Siehe newInstance
     * @throws IllegalAccessException Wenn einer der Setter oder der Konstruktor private ist. Siehe newInstance/setAttributes
     * @throws InstantiationException Wenn das Instanziieren der Klasse fehltschlägt. Siehe newInstance
     * @throws NoSuchMethodException  Wenn ein Attribut keinen zugehörigen Setter hat. Siehe setAttributes
     * @throws java.lang.reflect.InvocationTargetException Wenn eine der aufgerufenen Methoden eine Exception wirft. Siehe newInstance/setAttributes
     * @throws javax.xml.xpath.XPathExpressionException Wenn ein identifier, oder ein Attribute(name oder value) eine XPath Injection enthält
     */
    private Object getObj(String identifier) throws XPathExpressionException, ClassNotFoundException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Object o = newInstance(identifier);
        Hashtable<String, String> attributes = getClassAttributes(identifier);
        setAttributes(o, attributes);
        return o;
    }

    /**
     * Liest die zu 'identifier' gehörenden Attribute aus der Konfigurationsdatei
     *
     * @param identifier Der Bezeichner der zur erzeugenden Instanz
     * @return Die Attributtabelle
     * @throws javax.xml.xpath.XPathExpressionException Wenn der identifier den XPath Ausdruck kaputt macht..
     */
    private Hashtable<String, String> getClassAttributes(String identifier) throws XPathExpressionException {
        Hashtable<String, String> attributes = new Hashtable<String, String>();
        //XPath Ausdruck gibt ein Nodeset mit den zu dem identifier gehörigen Attributen zurück
        String xpAttributes = "//class[@name='" + identifier + "']/attributes/attribute";
        NodeList nodes = (NodeList) xpath.evaluate(xpAttributes, doc, XPathConstants.NODESET);

        for (int i = 0; i < nodes.getLength(); i++) {
            Node n = nodes.item(i);
            NamedNodeMap nMap = n.getAttributes();
            attributes.put(nMap.getNamedItem("name").getNodeValue(), nMap.getNamedItem("value").getNodeValue());
        }
        return attributes;
    }

    /**
     * Erzeugt eine Instanz des zu 'identifier' gehörenden Objekts
     *
     * @param identifier Der Bezeichner unter dem der Klassenname zu finden ist
     * @return Das erzeugte Objekt
     * @throws ClassNotFoundException Wenn die in der Config angegebene Klasse nicht existiert
     * @throws IllegalAccessException Wenn der Konstruktor der angegebenen Klasse private ist
     * @throws InstantiationException Wenn der Konstruktor der angegebenen Klasse eien Exception wirft
     * @throws javax.xml.xpath.XPathExpressionException Wenn der identifier den XPath Ausdruck kaputt macht
     */
    private Object newInstance(String identifier) throws XPathExpressionException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        //XPath Ausdruck gibt den zu dem identifier gehoerenden Klassennamen zurück
        String xpClassName = "//class[@name='" + identifier + "']/@className";
        String className = xpath.evaluate(xpClassName, doc);
        Class c = Class.forName(className);
        return c.newInstance();
    }

    /**
     * Setzt die aus der Konfigurationsdatei ausgelesenen Attribute über ihre jeweiligen Setter (Nur String Parameter
     * zulässig)
     *
     * @param o          Das zu verändernde Objekt
     * @param attributes Die Attributtabelle
     * @throws ClassNotFoundException Wenn java.lang.String nicht existiert
     * @throws IllegalAccessException Wenn ein Attribut einen Setter hat der private ist
     * @throws NoSuchMethodException Wenn ein Attribute in der XML Config keinem zugehörigen Feld entspricht
     * @throws java.lang.reflect.InvocationTargetException Wenn der Methodenaufruf des Setters fehlschlägt
     */
    private void setAttributes(Object o, Hashtable<String, String> attributes) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, ClassNotFoundException {
        Class stringClass = Class.forName("java.lang.String");
        Class c = o.getClass();
        for (Map.Entry<String, String> entry : attributes.entrySet()) {
            Method m = c.getMethod("set" + entry.getKey(), stringClass);
            m.invoke(o, entry.getValue());
        }
    }

}

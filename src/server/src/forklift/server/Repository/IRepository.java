package forklift.server.Repository;

import forklift.common.Order;
import forklift.common.Status;

import java.util.Iterator;
import java.util.Set;

/**
 * Schnittstelle für die Datenhaltung
 *
 * @author David Klein
 */
public interface IRepository {
    /**
     * Initialisiert die Verbindung
     */
    public void initialize();

    /**
     * Schließt die Verbindung
     */
    public void shutdown();

    /**
     * Speichert einen neuen Auftrag ab
     *
     * @param o Der zu speichernde Auftrag
     */
    public void persist(Order o);

    /**
     * Setzt zu dem übergebenen Auftrag den aktuellen Status auf s
     *
     * @param o Der betroffene Auftrag
     * @param s Der neue Status
     */
    public void updateStatus(Order o, Status s);

    /**
     * Reserviert eine neue Auftragsnummer für einen zu erzeugenden Auftrag
     *
     * @return Die Auftragsnummer
     */
    public int getNextId();

    /**
     * Gibt alle gespeicherten Aufträge zurück
     *
     * @return Die Auftragsmenge
     */
    public Iterator<Order> getOrders();

    /**
     * Gibt alle gespeicherten Aufträge mit dem Status s zurück
     *
     * @param s Der Status nach dem gefiltert werden soll
     * @return Die betreffenden Aufträge
     */
    public Iterator<Order> getOrdersByStatus(Status s);

    /**
     * Gibt den Auftrag mit der Auftragsnummer id zurück (sofern vorhanden)
     *
     * @param id Die Auftragsnummer nach der gesucht wird
     * @return Der betreffende Auftrag oder null
     */
    public Order getOrderById(int id);



}

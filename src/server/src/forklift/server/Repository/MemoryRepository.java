package forklift.server.Repository;

import forklift.common.Order;
import forklift.common.Status;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * Eine Beispielimplementation von IRepository um das Programm ohne Datenbank testen zu können
 *
 * @author David Klein
 */
public class MemoryRepository implements IRepository {
    private int id;
    private final ArrayList<Order> queuedOrders;

    public MemoryRepository() {
        this.id = 0;
        this.queuedOrders = new ArrayList<Order>();

    }

    @Override
    public void persist(Order o) {
        this.queuedOrders.add(o);
    }

    @Override
    public void updateStatus(Order order, Status s) {
        for (Order o : this.queuedOrders) {
            if (o.getId() == order.getId()) {
                o.setStatus(s);
            }
        }
    }

    @Override
    public int getNextId() {
        return id++;
    }

    @Override
    public Iterator<Order> getOrders() {
        return this.queuedOrders.iterator();
    }

    @Override
    public Iterator<Order> getOrdersByStatus(Status s) {
        ArrayList<Order> filtered = new ArrayList<Order>();
        for (Order o : queuedOrders) {
            if (o.getStatus() == s) {
                filtered.add(o);
            }
        }
        return filtered.iterator();
    }

    @Override
    public Order getOrderById(int id) {
        for (Order o : queuedOrders) {
            if (o.getId() == id) {
                return o;
            }
        }
        return null;
    }


    @Override
    public void initialize() {
    }

    @Override
    public void shutdown() {
    }
}

package forklift.server.Repository;

import forklift.common.Position;

import java.util.HashSet;
import java.util.Set;


/**
 * Enthält die Ursprungskonfiguration der Paletten auf dem Grid
 *
 * @author David Klein
 */
public class DefaultCargoPositions {
    private Set<Position> currentPositions = null;

    public DefaultCargoPositions() {
        currentPositions = new HashSet<Position>();
    }

    /**
     * Setzt die Ursprungspositionen der Paletten
     * @param pos Eine Komma-separierte Liste von Positionswerten
     */
    public void setPositions(String pos) {
        String[] splitted = pos.split(",");
        for(String split : splitted) {
            currentPositions.add(Position.valueOf(split));
        }

    }

    /**
     * Gibt die Ursprungskonfiguration zurück
     * @return Ursprungskonfiguration als Set
     */
    public Set<Position> getCurrentPositions() {
        return currentPositions;
    }
}

package forklift.server.Repository;

import forklift.common.Order;
import forklift.common.Position;
import forklift.common.Status;

import java.sql.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * Verwaltet die Auftragsdaten in einer Datenbank
 *
 * @author David Klein
 */
public class DatabaseRepository implements IRepository {

    private Connection connection = null;
    private String driverName = null;
    private String connectionString = null;
    private final static Logger LOGGER = Logger.getLogger(DatabaseRepository.class.getName());

    public DatabaseRepository() {
    }

    /**
     * Setzt vorraus das driverName und connectionString gesetzt sind
     */
    @Override
    public void initialize() {
        try {
            Class.forName(driverName);
            connection = DriverManager.getConnection(connectionString);
        } catch (Exception ex) {
            LOGGER.severe(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }


    @Override
    public void shutdown() {
        try {
            connection.close();
        } catch (Exception ex) {
            LOGGER.severe(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }


    @Override
    public synchronized void persist(Order o) {
        try {
            PreparedStatement insertIntoExecutionOrder = connection.prepareStatement("Insert Into ExecutionOrder (OrderID) Values( ? )");
            insertIntoExecutionOrder.setInt(1, o.getId());
            insertIntoExecutionOrder.executeUpdate();
            insertIntoExecutionOrder.close();

            int sourceId = getIdForPosition(o.getSource());
            int destinationId = getIdForPosition(o.getDestination());
            //Da die ID vorher schon reserviert wurde -> Update
            PreparedStatement persistOrder = connection.prepareStatement("Update Orders Set SourceID = ?, DestinationId = ? Where Id = ?");
            persistOrder.setInt(1, sourceId);
            persistOrder.setInt(2, destinationId);
            persistOrder.setInt(3, o.getId());
            persistOrder.executeUpdate();
            updateStatus(o, Status.OPEN);
            persistOrder.close();
        } catch (Exception ex) {
            LOGGER.severe(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }


    @Override
    public synchronized void updateStatus(Order o, Status s) {
        try {
            int statusId = getIdForStatus(s);
            java.util.Date atm = new java.util.Date();
            Timestamp sqlAtm = new Timestamp(atm.getTime());
            PreparedStatement setUpdated = connection.prepareStatement("Insert Into Updated (OrderID, StatusID, Time) Values( ? , ? , ? )");
            setUpdated.setInt(1, o.getId());
            setUpdated.setInt(2, statusId);
            setUpdated.setTimestamp(3, sqlAtm);
            setUpdated.executeUpdate();
            setUpdated.close();

            //just in case..
            o.setStatus(s);
        } catch (SQLException ex) {
            LOGGER.severe(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }


    @Override
    public synchronized int getNextId() {
        try {
            int reservedPosition = getIdForPosition(Position.RE);
            PreparedStatement getNextId = connection.prepareStatement("Insert Into Orders (SourceID, DestinationID) Values( ? , ? )", Statement.RETURN_GENERATED_KEYS);
            getNextId.setInt(1, reservedPosition);
            getNextId.setInt(2, reservedPosition);
            getNextId.executeUpdate();
            ResultSet result = getNextId.getGeneratedKeys();
            result.next();
            int id = result.getInt(1);
            result.close();
            getNextId.close();

            //temporäre Order Instanz um den Status auf RESERVED setzen zu können
            Order temp = new Order(id, Position.RE, Position.RE);
            updateStatus(temp, Status.RESERVED);

            return id;
        } catch (Exception ex) {
            LOGGER.severe(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }


    @Override
    public synchronized Iterator<Order> getOrders() {
        ArrayList<Order> orders = new ArrayList<Order>();
        try {
            PreparedStatement getOrders = connection.prepareStatement("Select Id, Source, Destination, LastUpdate, Status from vOrders");
            ResultSet results = getOrders.executeQuery();
            while (results.next()) {
                int id = results.getInt("Id");
                Position source = Position.valueOf(results.getString("Source"));
                Position destination = Position.valueOf(results.getString("Destination"));
                //java.util.Date lastUpdate = new java.util.Date(results.getTimestamp("LastUpdate").getTime());
                java.util.Date lastUpdate = results.getDate("LastUpdate");
                //System.err.println(results.getString("Status"));
                Status status = Status.valueOf(results.getString("Status"));

                Order o = new Order(id, source, destination);
                o.setStatus(status);
                o.setUpdated(lastUpdate);
                orders.add(o);
            }
        } catch (Exception ex) {
            LOGGER.severe(ex.getMessage());
            throw new RuntimeException(ex);
        }

        return orders.iterator();
    }

    @Override
    public Iterator<Order> getOrdersByStatus(Status s) {
        ArrayList<Order> filtered = new ArrayList<Order>();
        Iterator<Order> orders = getOrders();
        while (orders.hasNext()) {
            Order o = orders.next();
            if (o.getStatus() == s) {
                filtered.add(o);
            }
        }
        return filtered.iterator();
    }

    @Override
    public Order getOrderById(int id) {
        Order order = null;
        Iterator<Order> orders = getOrders();
        while (orders.hasNext()) {
            Order o = orders.next();
            if (o.getId() == id) {
                order = o;
            }
        }
        return order;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    private int getIdForStatus(Status s) throws SQLException {
        PreparedStatement getId = connection.prepareStatement("SELECT ID FROM Status where Description = ?");
        getId.setString(1, s.toString());
        ResultSet result = getId.executeQuery();
        int id;
        if (result.next()) {
            id = result.getInt(1);
        } else {
            throw new RuntimeException("Statustabelle nicht ordnungsgemaess initialisiert");
        }
        result.close();
        getId.close();
        return id;
    }

    private int getIdForPosition(Position p) throws SQLException {
        PreparedStatement getId = connection.prepareStatement("SELECT ID FROM Position where Description = ?");
        getId.setString(1, p.toString());
        ResultSet result = getId.executeQuery();
        int id;
        if (result.next()) {
            id = result.getInt(1);
        } else {
            throw new RuntimeException("Positionstabelle nicht ordnungsgemaess initialisiert");
        }
        result.close();
        getId.close();
        return id;

    }

}

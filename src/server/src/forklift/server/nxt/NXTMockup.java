package forklift.server.nxt;

import dak.lego.DeviceInfo;
import forklift.common.Position;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Eine Beispielimplementation von INXT um das Programm auch ohne Bluetooth/NXT Hardware testen zu können
 * @author David Klein
 */
public class NXTMockup implements INXT {
    private final static Logger LOGGER = Logger.getLogger(NXTMockup.class.getName());
    private static int mailbox6RecCounter = 0;
    private static int mailbox5RecCounter = 0;


    @Override
    public void establishConnection() {
        //nothing to do
    }

    @Override
    public String receiveMessage(int mailbox) throws IOException {
        LOGGER.info("receiveMessage - " + mailbox + ": (" + mailbox5RecCounter + "," + mailbox6RecCounter + ")");
        Position[] positions = {Position.A1, Position.B4, Position.C5, Position.H6, Position.A6};
        //Die Position des Gabelstaplers
        if(mailbox == 6) {
            Position p = positions[mailbox6RecCounter%positions.length];
            mailbox6RecCounter++;
            return p.toString();
        }
        //Alle 20 Aufrufe wird der aktuelle Auftrag quittiert
        if(mailbox == 5) {
            if((mailbox5RecCounter%20) == 5) {
                mailbox5RecCounter++;
                return "Job completed";
            }
            else {
                mailbox5RecCounter++;
                return "";
            }
        }
        throw new IllegalStateException("Es werden nur Mailbox 5 und 6 benutzt");
    }

    @Override
    public void sendMessage(int mailbox, String cargo) throws IOException {
        //nothing to do here
    }

    @Override
    public NXTInfo readNXTInfo() throws IOException {
        NXTInfo info = new NXTInfo();
        float batteryLevel = 31.5f;
        float[] firmwareVersion = new float[] {1.2f,2.0f};
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setName("Mockup!");
        deviceInfo.setBlueToothAddress("\u00af\\_(\u30c4)_/\u00af");
        deviceInfo.setBlueToothSignalStrength(5);
        deviceInfo.setFreeUserFlash(3);

        info.setBatteryLevel(batteryLevel);
        info.setFirmwareVersion(firmwareVersion);
        info.setDeviceInfo(deviceInfo);

        return info;
    }
}

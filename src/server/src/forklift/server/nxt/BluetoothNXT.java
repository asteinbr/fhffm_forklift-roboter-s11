package forklift.server.nxt;

import com.sun.servicetag.SystemEnvironment;
import dak.lego.DeviceInfo;
import dak.lego.NxtControl;

import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import java.io.IOException;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

/**
 * Stellt die Kommunikation mit dem NXT via Bluetooth bereit
 *
 * @author David Klein
 */
public class BluetoothNXT implements INXT {
    private String bluetoothAddress = "";
    private NxtControl ctrl = null;
    //Da von verschiedenen Threads auf den NXT zugegriffen wird, verhindert das Lock Race Conditions
    private final ReentrantLock lock = new ReentrantLock();
    private final static Logger LOGGER = Logger.getLogger(BluetoothNXT.class.getName());

    public void setBluetoothAddress(String bluetoothAddress) {
        this.bluetoothAddress = bluetoothAddress;
    }

    @Override
    public void establishConnection() {
        if (bluetoothAddress.equals("")) {
            LOGGER.severe("Bluetooth Addresse nicht gesetzt");
            throw new IllegalStateException("Bluetooth Addresse nicht gesetzt");
        }
        try {
            StreamConnection sc;
            String connURL = "btspp://" + this.bluetoothAddress + ":1";
            sc = (StreamConnection) Connector.open(connURL);
            ctrl = new NxtControl(sc.openOutputStream(), sc.openInputStream());
        } catch (Exception ex) {
            LOGGER.severe(ex.getMessage());
            throw new RuntimeException("Verbindung zum NXT konnte nicht aufgebaut werden");
        }

    }

    @Override
    public String receiveMessage(int mailbox) throws IOException {
        lock.lock();
        try {
            return ctrl.receiveMessage(mailbox);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void sendMessage(int mailbox, String cargo) throws IOException {
        lock.lock();
        try {
            ctrl.sendMessage(mailbox, cargo);
        } finally {
            lock.unlock();
        }

    }

    @Override
    public NXTInfo readNXTInfo() throws IOException {
        lock.lock();
        NXTInfo info = new NXTInfo();
        try {
            float batteryLevel = ctrl.getBatteryLevel();
            float[] firmwareVersion = ctrl.getFirmwareVersion();
            DeviceInfo deviceInfo = ctrl.getDeviceInfo();
            info.setBatteryLevel(batteryLevel);
            info.setFirmwareVersion(firmwareVersion);
            info.setDeviceInfo(deviceInfo);
            return info;
        } finally {
            lock.unlock();

        }


    }
}

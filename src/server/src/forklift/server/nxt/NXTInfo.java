package forklift.server.nxt;

import dak.lego.DeviceInfo;

/**
 * Enthält verschiedene Informationen über den NXT
 *
 * @author David Klein
 */
public class NXTInfo {
    private float batteryLevel;
    private float[] firmwareVersion;
    private DeviceInfo deviceInfo;

    public float getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(float batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public float[] getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(float[] firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }
}

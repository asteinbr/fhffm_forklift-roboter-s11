package forklift.server.nxt;

import java.io.IOException;

/**
 * Schnittstelle für die Kommunikation mit dem NXT
 *
 * @author David Klein
 */
public interface INXT {

    public void establishConnection();

    /**
     * Liest eine Nachricht vom NXT aus und löscht diese aus der Mailbox
     * @param mailbox In welcher mailbox (0..9) befindet sich die Nachricht?
     * @return Die Nachricht, "" falls die Mailbox leer ist
     * @throws IOException Wenn das Lesen vom NXT fehlschlägt
     */
    public String receiveMessage(int mailbox) throws IOException;

    /**
     * Sendet eine Nachricht an den NXT
     * @param mailbox Die Zielmailbox (0..9)
     * @param cargo Die Nachricht
     * @throws IOException Wenn das Senden an den NXT fehlschlägt
     */
    public void sendMessage(int mailbox, String cargo) throws IOException;

    /**
     * Liest die Statusinformationen des NXTs aus
     * @return Die Statusinformationen
     * @throws IOException Wenn das auslesen der NXTInfo fehlschlägt
     */
    public NXTInfo readNXTInfo() throws IOException;
}

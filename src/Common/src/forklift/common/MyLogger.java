package forklift.common;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;
import java.util.logging.Logger;

/**
 * Klasse die die globalen Loggin Informationen initialisiert
 * @author David Klein
 */
public class MyLogger {
    /**
     * Initialisiert die globale Logging Konfiguration
     * @throws IOException Wenn Logging.txt nicht beschreibbar ist
     */
    public static void setup() throws IOException {
       	Logger logger = Logger.getLogger("");
		logger.setLevel(Level.SEVERE);
        FileHandler fileTxt = new FileHandler("Logging.txt");
        SimpleFormatter formatterTxt = new SimpleFormatter();

		fileTxt.setFormatter(formatterTxt);
		logger.addHandler(fileTxt);
    }
}

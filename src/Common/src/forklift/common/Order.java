package forklift.common;

import java.util.Date;

/**
 * Representiert einen Auftrag den der Gabelstapler abarbeiten soll
 * @author David Klein
 */
public class Order {
    private int id;
    private Position source;
    private Position destination;
    private Status status;
    private Date updated;

    public Order() {}

    public Order(int orderId, Position sourcePosition, Position destinationPosition) {
        updated = new Date();
        id = orderId;
        source = sourcePosition;
        destination = destinationPosition;
        status = Status.OPEN;
    }

    public int getId() {
        return id;
    }

    public Position getSource() {
        return source;
    }

    public Position getDestination() {
        return destination;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status s) {
        this.status = s;
    }

    public void setUpdated(Date d) {
        this.updated = d;
    }

    public Date getLastUpdateDate() {
        return this.updated;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(id);
        sb.append(": ");
        sb.append(source.toString());
        sb.append("/");
        sb.append(destination.toString());
        sb.append(" (");
        sb.append(updated.toString());
        sb.append(") ");
        sb.append(status.toString());

        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Order)) {
            return false;
        }
        Order o = (Order) obj;
        return (this.id == o.id) && (this.source == o.source) && (this.destination == o.destination) && (this.status == o.status) && (this.updated == o.updated);
    }

    @Override
    public int hashCode() {
        return this.id;
    }
}

package forklift.common;

/**
 * Die Verschiedenen Zustände die ein Auftrag annehmen kann
 * @author David Klein
 */
public enum Status {
    RESERVED, // Für Aufträge die beim Client in der Staging Area liegen, aber noch nicht übermittelt wurden
    OPEN, // Offene Aufträge
    ACCEPT, // Aktuell in Bearbeitung
    DONE // Bearbeitung abgeschlossen
}

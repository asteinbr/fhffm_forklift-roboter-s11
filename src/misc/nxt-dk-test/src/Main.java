/*import dak.lego.NxtControl;

import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import java.io.IOException;  */

import java.io.IOException;

/**
 * Testklasse um die Bluetoothkommunikation mit dem NXT zu testen
 */
public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println(convert("¯\\_(ツ)_/¯"));
        /*StreamConnection sc;
	    NxtControl ctrl;
        String connURL = "btspp://0016530BEBC8:1"; // channel number (1,2,3 for slave NXTs)
		System.err.println("using url : "+connURL);
		sc = (StreamConnection) Connector.open(connURL);
		ctrl = new NxtControl(sc.openOutputStream(), sc.openInputStream());
        System.out.println("--");
        int i=0;
        while(i>=0) {
            ctrl.sendMessage(0,"A" + Integer.toString(i));
            i++;
            String resp = ctrl.receiveMessage(4);
            if(!resp.equals("")) {
                System.out.println(resp);
            }
            Thread.sleep(5000);
        }

        /*while(true) {
            String resp = ctrl.receiveMessage(4);
            if(!resp.equals("")) {
                System.out.println(resp + ": " + resp.length());
            }
        } */
    }

    public static String convert(String str) {

        StringBuffer ostr = new StringBuffer();

        for (int i = 0; i < str.length(); i++)

        {

            char ch = str.charAt(i);

            if ((ch >= 0x0020) && (ch <= 0x007e)) // Does the char need to be converted to unicode?

            {

                ostr.append(ch); // No.

            } else // Yes.
            {

                ostr.append("\\u"); // standard unicode format.

                String hex = Integer.toHexString(str.charAt(i) & 0xFFFF); // Get hex value of the char.

                for (int j = 0; j < 4 - hex.length(); j++) // Prepend zeros because unicode requires 4 digits

                    ostr.append("0");

                ostr.append(hex.toLowerCase()); // standard unicode format.

//ostr.append(hex.toLowerCase(Locale.ENGLISH));

            }

        }

        return (new String(ostr)); //Return the stringbuffer cast as a string.


    }
}

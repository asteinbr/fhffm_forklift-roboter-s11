
filename = "../Common/src/forklift/common/Position.java"
package = "forklift.common"

fieldSize = 8
file = open(filename, 'w')
file.write("//Automatisch generiertes File, bitte misc/genPosition.py mit korrekten Parametern aufrufen bzw. anpassen\n\n")
file.write("package " + package +";\n\n")

file.write("\nimport java.util.ArrayList;\nimport java.util.Iterator;\n")
file.write("public enum Position {" +"\n")
file.write("\tRE,\n")

for i in range(1,fieldSize+1):
	for j in range(1,fieldSize+1):
		c = chr(ord('A')+i-1)
		file.write("\t" + str(c)+str(j)+",")
	file.write("\n")
file.write("\t;")
file.write("\n\n")
file.write("\t//Nur Randfelder sind gueltig\n")
file.write("\tpublic static boolean isValidSourceOrDestination(Position pos) {\n")
file.write("\t\treturn pos.toString().contains(\"A\") || pos.toString().contains(\"1\") || pos.toString().contains(\"" + str(fieldSize) + "\") || pos.toString().contains(\"" + str(chr(ord('A')+fieldSize-1)) + "\");\n")
file.write("\t}\n")
file.write("\tpublic static Iterator<Position> getValidPositions() {\n")
file.write("\t\tArrayList<Position> validPositions = new ArrayList<Position>();\n")
file.write("\t\tfor(Position p : Position.values()) {\n")
file.write("\t\t\tif(Position.isValidSourceOrDestination(p)) {\n")
file.write("\t\t\t\tvalidPositions.add(p);\n")
file.write("\t\t\t}\n")
file.write("\t\t}\n")
file.write("\t\treturn validPositions.iterator();\n")
file.write("\t}\n")
file.write("}")
file.close()

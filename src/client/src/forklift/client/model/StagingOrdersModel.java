package forklift.client.model;

import forklift.common.Order;
import forklift.common.Position;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Alexander Steinbrecher
 * Date: 5/11/11
 * Time: 7:34 PM
 */

/**
 * Klasse dient zur Verwaltung der Aufträge des Dialogs 'neue Aufträge erfassen'
 *
 * @author Alexander Steinbrecher
 */
public class StagingOrdersModel implements TableModel {

    private ArrayList<Order> stagingOrders = new ArrayList<Order>();
    private ArrayList<Order> tempOrders = new ArrayList<Order>();
    private ArrayList listeners = new ArrayList();

    /**
     * Fügt ein übergebenene Auftrag 'order' einer ArrayList 'stagingOrders' hinzu
     *
     * @param order ArrayList mit Aufträgen
     */
    public void addStagingOrder(Order order) {
        // cout staging orders
        int id = stagingOrders.size();

        // add order to arraylist stagingOrders
        stagingOrders.add(order);

        // create and send event to create new row
        TableModelEvent tme = new TableModelEvent(this, id, id,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.INSERT);
        for (int i = 0, n = listeners.size(); i < n; i++) {
            ((TableModelListener)listeners.get(i)).tableChanged(tme);
        }
    }

    /**
     * Entfernt ein Auftrag anhand der Positionsnummer innerhalb der ArrayList
     *
     * @param selectedRow Position des Auftrags in der ArrayList
     */
    public void removeStagingOrder(int selectedRow) {
        stagingOrders.remove(selectedRow);

        // create and send event to remove selected row
        TableModelEvent tme = new TableModelEvent(this, selectedRow, selectedRow,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.DELETE);
        for (int i = 0, n = listeners.size(); i < n; i++) {
            ((TableModelListener)listeners.get(i)).tableChanged(tme);
        }
    }

    /**
     * Verschiebt ein Auftrag anhand seiner Positionsnummer in der ArrayList um eine Stelle nach oben und
     * aktualisiert den TableModelListener damit die Änderung sofort in der Tabelle sichtbar ist
     *
     * @param selectedRow Die Positionsnummer des Auftrags in der Tabelle
     */
    public void moveUp(int selectedRow) {
        for (int i = 0; i < stagingOrders.size(); i++) {
            if (i == selectedRow-1) {
                tempOrders.add(stagingOrders.get(selectedRow));
            } else if (i == selectedRow) {
                tempOrders.add(stagingOrders.get(i-1));
            } else {
                tempOrders.add(stagingOrders.get(i));
            }
        }

        stagingOrders.clear();
        stagingOrders = (ArrayList<Order>) tempOrders.clone();
        tempOrders.clear();

        // create and send event to update table
        TableModelEvent tme = new TableModelEvent(this, selectedRow, selectedRow,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.DELETE);
        for (int i = 0, n = listeners.size(); i < n; i++) {
            ((TableModelListener)listeners.get(i)).tableChanged(tme);
        }
    }

    /**
     * Verschiebt ein Auftrag anhand seiner Positionsnummer in der ArrayList um eine Stelle nach unten und
     * aktualisiert den TableModelListener damit die Änderung sofort in der Tabelle sichtbar ist
     *
     * @param selectedRow Die Positionsnummer des Auftrags in der Tabelle
     */
    public void moveDown(int selectedRow) {
        for (int i = 0; i < stagingOrders.size(); i++) {
            if (i == selectedRow+1) {
                tempOrders.add(stagingOrders.get(selectedRow));
            } else if (i == selectedRow) {
                tempOrders.add(stagingOrders.get(i+1));
            } else {
                tempOrders.add(stagingOrders.get(i));
            }
        }

        stagingOrders.clear();
        stagingOrders = (ArrayList<Order>) tempOrders.clone();
        tempOrders.clear();

        // create and send event to update table
        TableModelEvent tme = new TableModelEvent(this, selectedRow, selectedRow,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.DELETE);
        for (int i = 0, n = listeners.size(); i < n; i++) {
            ((TableModelListener)listeners.get(i)).tableChanged(tme);
        }
    }

    /**
     * Gibt die ArrayList 'stagingOrders' zurück
     *
     * @return Die ArrayList 'stagingOrders' mit temporären Aufträgen
     */
    public ArrayList<Order> getStagingOrders() {
        return stagingOrders;
    }

    /**
     * Ruft die Servermethode 'queueOrders(Order)' auf und übergibt alle Aufträge, die sich in 'stagingOrders'
     * befinden
     */
    public void queueOrders() {
        ServerImpl.getInstance().getServer().queueOrders(stagingOrders.iterator());
    }

    /**
     * Entfernt alle Auftrage in der ArrayList
     */
    public void clearStagingOrders() {
        for (int i = 0; i < stagingOrders.size(); i++) {
            removeStagingOrder(i);
        }
    }

    /**
     * Ruft Servermethode 'getOrderId()' auf, damit der neu angelegte Auftrag eine fortlaufende Nummer besitzt
     *
     * @return nächste nicht-vergebene Auftragsnummer
     */
    public String getOrderId() {
        Integer orderId = ServerImpl.getInstance().getServer().getOrderId();

        return orderId.toString();
    }

    /////////////////// TABLE MODEL ///////////////////
    // amount of rows
    public int getRowCount() {
        return stagingOrders.size();
    }

    // amount of columns
    public int getColumnCount() {
        return 3;
    }

    // names for columns
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0: return "ID";
            case 1: return "Source";
            case 2: return "Destination";
            default:return null;
        }
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0: return Integer.class;
            case 1: return String.class;
            case 2: return String.class;
            default:return null;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    // returns a value of a single cell
    public Object getValueAt(int rowIndex, int columnIndex) {
        // save appropriate Order to own Object
        Order order = (Order)stagingOrders.get(rowIndex);

        switch (columnIndex) {
            case 0: return order.getId();
            case 1: return order.getSource().toString();
            case 2: return order.getDestination().toString();
            default:return null;
        }
    }

    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }
}

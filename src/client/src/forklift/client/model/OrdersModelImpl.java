package forklift.client.model;

/**
 * Created by IntelliJ IDEA.
 * User: Alexander Steinbrecher
 * Date: 5/23/11
 * Time: 8:17 PM
 */

/**
 * Klasse stellt eine globale Instanz für das OrderModel dar
 * es wird das Singletone-Pattern verwendet
 *
 * @author Alexander Steinbrecher
 */
public class OrdersModelImpl {
    private static OrdersModelImpl ourInstance = new OrdersModelImpl();
    private OrdersModel ordersModel = null;

    public static OrdersModelImpl getInstance() {
        return ourInstance;
    }

    public OrdersModel getOrdersModel() {
        return ordersModel;
    }

    private OrdersModelImpl() {
        this.ordersModel = new OrdersModel();
    }
}

package forklift.client.model;

import forklift.server.Repository.IRepository;
import forklift.server.IServer;
import forklift.common.MyLogger;
import forklift.server.Configuration.XMLConfigurationBasedFactory;
import forklift.server.Forklift;
import forklift.server.Repository.DefaultCargoPositions;
import forklift.server.Server;
import forklift.server.nxt.INXT;

/**
 * Created by IntelliJ IDEA.
 * User: Alexander Steinbrecher
 * Date: 5/18/11
 * Time: 11:21 AM
 */

/**
 * Klasse stellt eine globale Instanz des Servers dar
 * es wird das Singletone-Pattern verwendet
 *
 * @author Alexander Steinbrecher
 */
public class ServerImpl {
    private final static ServerImpl ourInstance = new ServerImpl();
    private IServer server;
    private Forklift forklift;

    public static ServerImpl getInstance() {
        return ourInstance;
    }

    public synchronized void shutdown() {
        forklift.shutdown();
        server.shutdown();
    }

    public IServer getServer() {
        return server;
    }

    private ServerImpl() {
        /*XMLConfigurationBasedFactory c = new XMLConfigurationBasedFactory("src/server/config.xml");
        IRepository rep = c.getRepository();
        server = c.getServer();
        server.setRepository(rep);
        server.run(); */
        try {
            MyLogger.setup();
            XMLConfigurationBasedFactory c = new XMLConfigurationBasedFactory("config.xml");
            IRepository rep = c.getRepository();
            DefaultCargoPositions cargo = c.getCargoPositions();
            server = c.getServer();
            server.setDefaultCargoPositions(cargo.getCurrentPositions());
            server.setRepository(rep);
            server.run();

            forklift = new Forklift();
            INXT nxt = c.getNXT();
            forklift.setNxt(nxt);
            forklift.setServer((Server) server);
            forklift.start();
        } catch(Exception ex) {
            throw new RuntimeException(ex);
        }

    }
}

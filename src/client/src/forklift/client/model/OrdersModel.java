package forklift.client.model;

import forklift.client.Main;
import forklift.client.view.Manager;
import forklift.common.Order;
import forklift.common.Position;
import forklift.common.Status;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by IntelliJ IDEA.
 * User: Alexander Steinbrecher
 * Date: 5/13/11
 * Time: 11:40 PM
 */

/**
 * Klasse dient zur Verwaltung der Aufträge des Managers
 *
 * @author Alexander Steinbrecher
 */
public class OrdersModel implements TableModel {
    // internal usage
    private ArrayList<Order> orders = new ArrayList<Order>();
    private ArrayList listeners = new ArrayList();

    /**
     * Fügt ein übergebenene Auftrag 'order' einer ArrayList 'orders' hinzu
     *
     * @param order ArrayList mit Aufträgen
     */
    public void addOrder(Order order) {
        // count staging orders
        int id = orders.size();

        // add order to arraylist orders
        orders.add(order);

        // create and send event to create new row
        TableModelEvent tme = new TableModelEvent(this, id, id,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.INSERT);
        for (int i = 0, n = listeners.size(); i < n; i++) {
            ((TableModelListener) listeners.get(i)).tableChanged(tme);
        }
    }

    /**
     * Entfernt ein Auftrag anhand der Positionsnummer innerhalb der ArrayList
     *
     * @param selectedRow Position des Auftrags in der ArrayList
     */
    public void removeOrder(int selectedRow) {
        orders.remove(selectedRow);

        // create and send event to remove selected row
        TableModelEvent tme = new TableModelEvent(this, selectedRow, selectedRow,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.DELETE);
        for (int i = 0, n = listeners.size(); i < n; i++) {
            ((TableModelListener) listeners.get(i)).tableChanged(tme);
        }
    }

    /**
     * Entfernt alle Auftrage in der ArrayList
     */
    public void removeAllOrders() {
        // remove order, while order list is not empty
        while (!(orders.isEmpty())) {
            this.removeOrder(0);
        }
    }

    /**
     * Ruft Servermethode 'getOrders()' auf um alle aktuellen Aufträge zu erhalten und diese der ArrayList hinzuzufügen
     * Zuvor werden alle vorherigen Aufträge mit 'removeAllOrders()' entfernt
     */
    public void getOrders() {
        // remove current-orders
        synchronized (this) {
            this.removeAllOrders();

            Iterator<Order> it = ServerImpl.getInstance().getServer().getOrders();

            // get and add orders form server to ordersModel in Manager
            while (it.hasNext()) {
                OrdersModelImpl.getInstance().getOrdersModel().addOrder(it.next());
            }
        }

    }

    /**
     * Ruft Servermethode mit dem gewünschten Status 'getOrdersByStatus(Status status)' auf und fügt die Resultate in
     * die ArrayList ein
     * Zuvor werden alle vorherigen Aufträge mit 'removeAllOrders()' entfernt
     *
     * @param status Der zu suchende Status
     */
    public void getOrdersByStatus(String status) {
        // remove current-orders view
        if (!(orders.size() == 0))
            this.removeAllOrders();

        Status s = Status.valueOf(status);
        Iterator<Order> orderIterator = ServerImpl.getInstance().getServer().getOrdersByStatus(s);

        while (orderIterator.hasNext()) {
            OrdersModelImpl.getInstance().getOrdersModel().addOrder(orderIterator.next());
        }
    }

    // calls getOrderById(int) and refresh table model

    /**
     * Ruft Servermethode 'getOrderById(int id) mit der gewünschten Auftragsnummer auf und fügt das Resultat in die
     * ArrayList ein
     * Zuvor werden alle vorherigen Aufträge mit 'removeAllOrders()' entfernt
     *
     * @param orderId Die zu suchende Auftragsnummer
     */
    public void getOrderById(int orderId) {
        // remove current-orders view
        if (!(orders.size() == 0))
            this.removeAllOrders();

        Order order = ServerImpl.getInstance().getServer().getOrderById(orderId);
        OrdersModelImpl.getInstance().getOrdersModel().addOrder(order);
    }

    /////////////////// TABLE MODEL ///////////////////
    // amount of rows
    public int getRowCount() {
        return orders.size();
    }

    // amount of columns
    public int getColumnCount() {
        return 5;
    }

    // names for columns
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0: return "ID";
            case 1: return "Quelle";
            case 2: return "Ziel";
            case 3: return "Zeit";
            case 4: return "Status";
            default:return null;
        }
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0: return Integer.class;
            case 1: return String.class;
            case 2: return String.class;
            case 3: return String.class;
            case 4: return String.class;
            default:return null;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    // returns a value of a single cell
    public Object getValueAt(int rowIndex, int columnIndex) {
        // save appropriate Order to own Object
        try {
            Order order = (Order) orders.get(rowIndex);

            switch (columnIndex) {
                case 0:
                    return order.getId();
                case 1:
                    return order.getSource().toString();
                case 2:
                    return order.getDestination().toString();
                case 3:
                    return order.getLastUpdateDate().toString();
                case 4:
                    return order.getStatus().toString();
                default:
                    return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }
}

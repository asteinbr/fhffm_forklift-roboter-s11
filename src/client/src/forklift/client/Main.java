package forklift.client;

import forklift.client.view.Manager;

/**
 * Created by IntelliJ IDEA.
 * User: Alexander Steinbrecher
 * Date: 5/3/11
 * Time: 8:56 AM
 */

/**
 * Einsprungadresse des Programms
 *
 * @author Alexander Steinbrecher
 */
public class Main {
    public static void main(String[] args) {
        new Manager(820, 520, "Forklift S11 Manager");
    }
}

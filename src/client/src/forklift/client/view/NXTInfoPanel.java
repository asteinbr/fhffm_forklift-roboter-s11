package forklift.client.view;

import dak.lego.DeviceInfo;
import forklift.client.model.ServerImpl;
import forklift.server.nxt.NXTInfo;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.Timer;
import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: Alexander Steinbrecher
 * Date: 7/20/11
 * Time: 03:40 PM
 */

/**
 * Klasse erzeugt ein Panel mit Informationen des NXT-Roboters
 *
 * @author Alexander Steinbrecher
 */
public class NXTInfoPanel {
    private final static Logger LOGGER = Logger.getLogger(Manager.class.getName());
    NXTInfo nxtInfo = null;

    // battery progressbar, min / max values
    public static final int MINBATTERY = 0;
    public static final int MAXBATTERY = 100;
    private JProgressBar batteryProgressBar;

    // bluetooth strength progressbar, min / max values
    private static final int MINBTSTRENGTH = 0;
    private static final int MAXBTSTRENGTH = 10;
    private JProgressBar bluetoothStrengthBar;

    // JLabels
    private JLabel firmwareVersionContentLabel;
    private JLabel deviceInfoNameContentLabel;
    private JLabel deviceInfoBluetoothAddressContentLabel;
    private JLabel deviceInfoBluetoothStrengthContentLabel;
    private JLabel deviceInfoFreeFlashContentLabel;

    /**
     * Erzeugt das Hauptpanel für die NXT-Informationen
     */
    public NXTInfoPanel() {
        createNXTInfoOuterPanel();
        intervall();
    }

    /**
     * Ruft in einem Intervall von 2sec die Methode refreshNXTInfo() auf
     */
    private void intervall() {
        Timer timer = new Timer();
        timer.schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        refreshNXTInfo();
                    }
                },2000, 2000);
    }

    /**
     * Methode setzt aktuelle Werte in die Labels der NXT-Informationen
     */
    private void refreshNXTInfo() {
        nxtInfo = ServerImpl.getInstance().getServer().getNXTInfo();

        if (nxtInfo != null) {
            updateBatteryLevel(nxtInfo.getBatteryLevel() * 3);
            updateFirmwareVersion(nxtInfo.getFirmwareVersion());
            updateDeviceInfo(nxtInfo.getDeviceInfo());
        }
    }

    /**
     * Macht ein Update der Progressbar des Batteriezustands
     *
     * @param batteryLevel  Der aktuelle Batteriestand
     */
    private void updateBatteryLevel(float batteryLevel) {
            batteryProgressBar.setValue((int) batteryLevel);
    }

    /**
     * Macht ein Update des FirmwareVersion-JLabels
     *
     * @param firmwareVersion   Die aktuelle FirmwareVersion des NXT's
     */
    private void updateFirmwareVersion(float[] firmwareVersion) {
            String firmwareVersionString = String.valueOf(firmwareVersion[0]);

            firmwareVersionContentLabel.setText(firmwareVersionString);
    }

    /**
     * Macht ein Update der JLabels der DeviceInfos
     *
     * @param deviceInfo    Objekt mit diversen DeviceInfos
     */
    private void updateDeviceInfo(DeviceInfo deviceInfo) {
            deviceInfoNameContentLabel.setText(deviceInfo.getName());
            deviceInfoBluetoothAddressContentLabel.setText(deviceInfo.getBlueToothAddress());
            bluetoothStrengthBar.setValue(100 - deviceInfo.getBlueToothSignalStrength());
            deviceInfoFreeFlashContentLabel.setText(String.valueOf(
                    deviceInfo.getFreeUserFlash()));
    }

    ////////////////////// NXTInfo //////////////////////

    /**
     * Erzeugt ein äußeres Panel für die NXT-Infos
     * Dies wird benötigt um die NXT-Infos links auszurichten
     *
     * @return Das äußere NXTInfopanel
     */
    public JComponent createNXTInfoOuterPanel() {
        JPanel nxtInfoOuterPanel = new JPanel();
        nxtInfoOuterPanel.setBorder(BorderFactory.createTitledBorder("NXT-System Informationen"));
        nxtInfoOuterPanel.setLayout(new BoxLayout(nxtInfoOuterPanel, BoxLayout.PAGE_AXIS));

        nxtInfoOuterPanel.add(createNXTInfoPanel());

        return nxtInfoOuterPanel;
    }

    /**
     * Erzeugt ein Fortschrittsbalken für den Batterieladestand
     *
     * @return Die Battery-Value-Progressbar
     */
    private JComponent batteryProgressBar() {
        batteryProgressBar = new JProgressBar(MINBATTERY, MAXBATTERY);
        batteryProgressBar.setStringPainted(true);
        batteryProgressBar.setValue(0);

        return batteryProgressBar;
    }

    /**
     * Erzeugt ein Fortschrittsbalken für die Bluetooth-Signalstärke
     *
     * @return Die Bluetooth-Strength-Progressbar
     */
    private JComponent bluetoothStrengthBar() {
        bluetoothStrengthBar = new JProgressBar(MINBTSTRENGTH, MAXBTSTRENGTH);
        bluetoothStrengthBar.setStringPainted(true);
        bluetoothStrengthBar.setValue(0);

        return bluetoothStrengthBar;
    }

    /**
     * Erzeugt das NXT-Info-Panel
     *
     * @return Das NXT-Info-Panel
     */
    private JComponent createNXTInfoPanel() {
        JPanel nxtInfoOuterPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel nxtInfoPanel = new JPanel(new GridLayout(6, 2));
            nxtInfoOuterPanel.add(nxtInfoPanel);

        // device Name
        JLabel deviceInfoNameLabel = new JLabel("Name: ");
            nxtInfoPanel.add(deviceInfoNameLabel);
        deviceInfoNameContentLabel = new JLabel("\u00af\\_(\u30c4)_/\u00af");
            nxtInfoPanel.add(deviceInfoNameContentLabel);

         // firmware version
        JLabel firmwareVersionLabel = new JLabel("Firmware: ");
            nxtInfoPanel.add(firmwareVersionLabel);
        firmwareVersionContentLabel = new JLabel("\u00af\\_(\u30c4)_/\u00af");
            nxtInfoPanel.add(firmwareVersionContentLabel);

        // device freeflash
        JLabel deviceInfoFreeFlashLabel = new JLabel("Free Flash Slots: ");
            nxtInfoPanel.add(deviceInfoFreeFlashLabel);
        deviceInfoFreeFlashContentLabel = new JLabel("\u00af\\_(\u30c4)_/\u00af");
            nxtInfoPanel.add(deviceInfoFreeFlashContentLabel);

        // device BT Addr
        JLabel deviceInfoBluetoothAddressLabel = new JLabel("BT address.: ");
            nxtInfoPanel.add(deviceInfoBluetoothAddressLabel);
        deviceInfoBluetoothAddressContentLabel = new JLabel("\u00af\\_(\u30c4)_/\u00af");
            nxtInfoPanel.add(deviceInfoBluetoothAddressContentLabel);

        // device BT strength
        JLabel deviceInfoBluetoothStrengthLabel = new JLabel("BT strength: ");
            nxtInfoPanel.add(deviceInfoBluetoothStrengthLabel);
            nxtInfoPanel.add(bluetoothStrengthBar());

        // battery level
        JLabel batteryLevelLabel = new JLabel("Battery: ");
            nxtInfoPanel.add(batteryLevelLabel);
            nxtInfoPanel.add(batteryProgressBar());

        return nxtInfoOuterPanel;
    }
}

package forklift.client.view;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: Alexander Steinbrecher
 * Date: 5/12/11
 * Time: 12:20 PM
 */

/**
 * Klasse erzeugt Hinweis/Fehler-Meldungen
 *
 * @author Alexander Steinbrecher
 */
public class MessageDialogs {
    private JFrame dialogFrame = null;

    /*
       type:       1=error
                   2=advice/hint
       message:    message that should be displayed
    */
    public MessageDialogs(int type, String message) {

        String title = null;

        if (type == 1) {
            title = "Fehler";
            createErrorDialog(title, message);
        } else if (type == 2) {
            title = "Hinweis";
            createHintDialog(title, message);
        }
    }

    public void createErrorDialog(String title, String errorMsg) {
        JOptionPane.showMessageDialog(dialogFrame,
            errorMsg, title, JOptionPane.ERROR_MESSAGE);
    }

    public void createHintDialog(String title, String message) {
        JOptionPane.showMessageDialog(dialogFrame,
            message, title, JOptionPane.INFORMATION_MESSAGE);
    }
}

package forklift.client.view;

import dak.lego.DeviceInfo;
import forklift.client.model.ServerImpl;
import forklift.common.Position;
import forklift.server.nxt.NXTInfo;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: Alexander Steinbrecher
 * Date: 5/16/11
 * Time: 10:29 PM
 */

/**
 * Klasse erzeugt ein Panel, welches das Positionsraster sowie die Positionslegende enthält
 *
 * @author Alexander Steinbrecher
 */
public class PositionsPanel {
    // datastructures to save elements from Positions.java
    private ArrayList<String> positionList = new ArrayList<String>();
    private String[] positionStringArray = new String[positionList.size()];

    // variable for amount of positions in one line
    private int positionMatrix;

    // datastructures to save buttons
    private ArrayList<JButton> buttonList = new ArrayList<JButton>();
    private Color defaultColor;

    /**
     * Konstruktor für das Positionsraster welches im Hauptdialog integriert wird
     */
    public PositionsPanel() {
        // save positions to arraylist
        getPositions();
    }

    /**
     * Erzeugt das äußere Positionspanel
     *
     * @return Das äußere Positionspanel
     */
    public JComponent createPositionPanel() {
        JPanel positionPanel = new JPanel();
        positionPanel.setLayout(new BoxLayout(positionPanel, BoxLayout.PAGE_AXIS));
        positionPanel.setBorder(BorderFactory.createTitledBorder("Paletten Positionen"));

        // call position-grid to include
        positionPanel.add(createPositionGrid());
        // call common-panel to include
        positionPanel.add(createPositionGridLegendOuterPanel());

        return positionPanel;
    }

    /////////////////// PositionGrid ///////////////////

    /**
     * Erzeugt das Positions dynamisch anhand der verfügbaren Positionen
     *
     * @return Ein Panel in dem das Positionsgrid eingebettet ist
     */
    public JComponent createPositionGrid() {
        JPanel positionGrid = new JPanel();

        //positionGrid.setMaximumSize(new Dimension(400, 0));
        positionGrid.setLayout(new GridLayout(positionMatrix, positionMatrix));
        for (int i = 0; i < positionList.size(); i++) {
            JButton positionButton = new JButton(positionStringArray[i]);
            // set action command to positionButton
            //    e.g. 'pos_A4' or 'pos_B1'
            positionButton.setActionCommand("pos_" + positionStringArray[i]);
            positionButton.setEnabled(false);
            positionButton.setFont(new Font("Monospaced", Font.ITALIC, 8));
            defaultColor = positionButton.getBackground();

            // add positionButton to arraylist (for reuse)
            buttonList.add(positionButton);

            positionGrid.add(positionButton);
        }

        return positionGrid;
    }

    /**
     * Ruft Servermethode 'getCargoPositions()' auf und iteriert die Resultate auf die lokale Methode
     * 'setCargoPosition(Position)' auf
     */
    public void getCargoPositions() {
        Iterator<Position> positionIterator = ServerImpl.getInstance().getServer().getCargoPositions();
        for (JButton button : buttonList) {
            button.setBackground(defaultColor);
        }
        // iterate results on setCargoPosition
        while (positionIterator.hasNext())
            setCargoPosition(positionIterator.next());
    }

    /**
     * Ruft Servermethode 'getForkliftPosition()' und ruft mit dem Resultat die lokale Methode
     * 'setForkliftPosistion()' auf
     */
    public void getForkliftPosition() {
        setForkliftPosition(ServerImpl.getInstance().getServer().getForkliftPosition());
    }

    /**
     * Setzt die Position des Roboter im Positionsraster fest
     *
     * @param forkliftPosition Die Position des Roboter
     */
    public void setForkliftPosition(Position forkliftPosition) {
        int pos = convertStringPosToIntPos(forkliftPosition.toString());
        JButton forkliftPositionButton = buttonList.get(pos);

        // set color
        forkliftPositionButton.setBackground(Color.GREEN);
    }

    /**
     * Setzt die Positionen des Cargos im Positionsraster fest
     *
     * @param cargoPosition Die Position des Cargo
     */
    public void setCargoPosition(Position cargoPosition) {
        int pos = convertStringPosToIntPos(cargoPosition.toString());
        JButton cargoPositionButton = buttonList.get(pos);

        // set color
        cargoPositionButton.setBackground(Color.YELLOW);
    }

    /**
     * Konvertiert eine Position zur Positionszahl
     *
     * @param position Eine Position
     * @return Die entsprechende Positionszahl der Position als Integer
     */
    private int convertStringPosToIntPos(String position) {
        for (int i = 0; i < positionStringArray.length; i++) {
            if (position.equals(positionStringArray[i]))
                return i;
        }

        return -1;
    }

    /**
     * Speichert alle verfügbaren Positionen in einem String Array ab
     *
     * @return Das String-Array mit allen verfügbaren Positionen
     */
    private String[] getPositions() {
        // saves all values of Positions to positionList
        for (Position p : Position.values()) {
            // prove if position isn't reserved
            if (!(p.toString().equals("RE")))
                positionList.add(p.toString());
        }

        positionStringArray = positionList.toArray(positionStringArray);

        // set positions in one line to positionMatrix
        positionMatrix = (int) Math.sqrt(positionList.size());

        return positionStringArray;
    }

    /**
     * Speichert alle validen Cargo-Positionen in einem String Array ab
     *
     * @return Das String-Array mit allen validen Cargo-Positionen
     */
    public static String[] getValidPositions() {
        Iterator<Position> positionIterator = Position.getValidPositions();
        ArrayList<String> positionArrayList = new ArrayList<String>();
        String[] validPositionStringArray = new String[positionArrayList.size()];

        // save iterated Positions to arraylist
        while (positionIterator.hasNext()) {
            positionArrayList.add(positionIterator.next().toString());
        }

        // save arraylist items to string array
        validPositionStringArray = positionArrayList.toArray(validPositionStringArray);

        return validPositionStringArray;
    }


    /////////////////// PositionGrid-Legende ///////////////////

    /**
     * Erzeugt die Legende für die Positionsfarben
     *
     * @return Ein Panel, in dem die Positionslegende eingebettet ist
     */
    private JComponent createPositionGridLegend() {
        JPanel positionGridLegend = new JPanel();
        positionGridLegend.setLayout(new GridLayout(2, 2));

        // Label - Legende
        JLabel legendeLabel = new JLabel("Legende:");
        positionGridLegend.add(legendeLabel);

        // Button - Roboter
        JButton roboterButton = new JButton("Roboter");
            roboterButton.setFont(new Font("Dialog", Font.ITALIC, 9));
            roboterButton.setBackground(Color.GREEN);
            roboterButton.setEnabled(false);
        positionGridLegend.add(roboterButton);

        // Label - space
        JLabel spaceLabel = new JLabel("");
            positionGridLegend.add(spaceLabel);

        // Button - Palette
        JButton cargoButton = new JButton("Palette");
            cargoButton.setFont(new Font("Dialog", Font.ITALIC, 9));
            cargoButton.setBackground(Color.YELLOW);
            cargoButton.setEnabled(false);
        positionGridLegend.add(cargoButton);

        return positionGridLegend;
    }

    /**
     * Erzeugt ein äußeres Panel für die Legende
     * Dies wird benötigt um die Legende links auszurichten
     *
     * @return Das äußere Positionslegendenpanel
     */
    public JComponent createPositionGridLegendOuterPanel() {
        JPanel positionGridLegendOuterPanel = new JPanel();
        positionGridLegendOuterPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        positionGridLegendOuterPanel.add(createPositionGridLegend());

        return positionGridLegendOuterPanel;
    }

}

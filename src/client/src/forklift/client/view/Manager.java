package forklift.client.view;

import forklift.client.model.OrdersModel;
import forklift.client.model.OrdersModelImpl;
import forklift.client.model.ServerImpl;
import forklift.server.IServer;
import forklift.common.Status;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: Alexander Steinbrecher
 * Date: 5/3/11
 * Time: 10:50 PM
 */

/**
 * Diese Klasse dient zur Erzeugung des Hauptframes der Anwendung
 */
public class Manager {

    private IServer server;
    //Wird zum Beenden von intervallCall() benutzt
    private volatile boolean running = true;
    private final static Logger LOGGER = Logger.getLogger(Manager.class.getName());

    // datastructures to save elements from Positions.java
    private ArrayList<String> statusList = new ArrayList<String>();
    private String[] statusLabels = new String[statusList.size()];

    // definition of menutoolbar
    private JMenu m_file = new JMenu("Datei");
    private JMenuItem mi_quit = new JMenuItem("Schlie\u00dfen");
    private JMenu m_tasks = new JMenu("Aufgaben");
    private JMenuItem mi_collectOrders = new JMenuItem("Auftr\u00e4ge erfassen");
    private JMenu m_info = new JMenu("?");
    private JMenuItem mi_info = new JMenuItem("Info");
    private JButton buttonCollectOrders = new JButton("neue Auftr\u00e4ge erfassen");

    private JTextField orderIdSearchField;
    private JComboBox orderStatusBox;

    // definition of table attributes
    private OrdersModel model;
    private JTable table;

    PositionsPanel positionsPanel;
    NXTInfoPanel nxtInfoPanel;

    /**
     * Konstruktor für den Hauptdialog der Anwendung
     *
     * @param width       Die Breite des Frame
     * @param height      Die Höhe des Frame
     * @param titleWindow Titel des Frame
     */
    public Manager(int width, int height, String titleWindow) {
        // set up tablemodel and jtable
        this.model = OrdersModelImpl.getInstance().getOrdersModel();
        this.table = new JTable(model);

        // create mainframe
        JFrame mainFrame = new JFrame();
        mainFrame.setTitle(titleWindow);
        mainFrame.setSize(width, height);

        // center window-position
        mainFrame.setLocationRelativeTo(null);

        // ui options
        mainFrame.setJMenuBar(createMenuToolbar());
        mainFrame.add(createMainPanel());
        mainFrame.setVisible(true);

        // Beim Beenden des Clients wird die Datenbankverbindung und die Backgroundthreads zur NXT-BT Kommunikation beendet
        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public synchronized void windowClosing(WindowEvent we) {
                running = false;
                ServerImpl.getInstance().shutdown();
                System.exit(0);
            }
        });
        this.server = ServerImpl.getInstance().getServer();
        // intervall
        intervallCall();
    }

    /**
     * Erzeugt das Hauptpanel für das Hauptframe
     *
     * @return Das Hauptpanel
     */
    private JComponent createMainPanel() {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.LINE_AXIS));

        // add content to panel
        mainPanel.add(createLeftOrdersPanel());
        mainPanel.add(createCommonRightPanel());

        return mainPanel;
    }

    /**
     * Erzeugt das Commonpanel, welches das Positionspanel sowie NXT-Info-Panel erzeugt
     *
     * @return Das Commonpanel
     */
    private JComponent createCommonRightPanel() {
        //JPanel createCommonRightPanel = new JPanel(new GridLayout(2, 1));
        JPanel commonRightPanel = new JPanel();
        commonRightPanel.setLayout(new BoxLayout(commonRightPanel, BoxLayout.PAGE_AXIS));

        // call positionpanel to include
        positionsPanel = new PositionsPanel();
        commonRightPanel.add(positionsPanel.createPositionPanel(), BorderLayout.LINE_START);
        // call NXT-Info-Panel to include
        nxtInfoPanel = new NXTInfoPanel();
        commonRightPanel.add(nxtInfoPanel.createNXTInfoOuterPanel(), BorderLayout.LINE_START);

        return commonRightPanel;
    }

    /**
     * Erzeugt das Panel für Aufträge
     *
     * @return Das Auftragspanel
     */
    private JComponent createLeftOrdersPanel() {
        JPanel ordersPanel = new JPanel();
        //ordersPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
        ordersPanel.setLayout(new BoxLayout(ordersPanel, BoxLayout.PAGE_AXIS));
        ordersPanel.setBorder(BorderFactory.createTitledBorder("Auftr\u00e4ge"));

        // call table and buttons to include
        ordersPanel.add(createOrdersSelectionOuterPanel());
        ordersPanel.add(createOrdersTable());
        ordersPanel.add(createButtonCollectOrdersPanel());

        return ordersPanel;
    }

    /**
     * Erzeugt ein Buttonpanel zur Erfassung neuer Aufträge
     *
     * @return Das Buttonpanel
     */
    private JComponent createButtonCollectOrdersPanel() {
        JPanel buttonCollectOrdersPanel = new JPanel();

        buttonCollectOrdersPanel.add(buttonCollectOrders);

        return buttonCollectOrdersPanel;
    }

    /**
     * Erzeugt ein äußeres Panel für die Suchfelder
     * Dies wird benötigt um die Suchfelder rechts auszurichten
     *
     * @return Das äußere Suchfelderpanel
     */
    private JComponent createOrdersSelectionOuterPanel() {
        JPanel ordersSelectionOuterPanel = new JPanel();
        ordersSelectionOuterPanel.setPreferredSize(new Dimension(400, 50));
        ordersSelectionOuterPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

        ordersSelectionOuterPanel.add(createOrdersSelectionPanel());

        return ordersSelectionOuterPanel;
    }

    /**
     * Erzeugt Panel für Suchfelder
     *
     * @return Das Suchfeldpanel
     */
    private JComponent createOrdersSelectionPanel() {
        JPanel ordersSelectionPanel = new JPanel();

        // panel for searchfield
        // flowlayout to justified components to right
        JPanel orderIdPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JLabel orderIdSearchLabel = new JLabel("Auftragsnr.:");

        // action handling for textfield
        orderIdSearchField = new JTextField(3);
        orderIdSearchField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JTextField tf = (JTextField) e.getSource();

                // wenn Auftragsnummerfeld leer ist, werden alle Aufträge angezeigt,
                // sonst wird der eingegebene Auftrag angezeigt
                if (tf.getText().isEmpty()) {
                    model.getOrders();
                } else {
                    model.getOrderById(Integer.parseInt(tf.getText()));
                    LOGGER.info("client - Manager > call getOrderById(" + tf.getText() + ")");
                    LOGGER.info("orderId changed to " + tf.getText());
                }
            }
        });

        // add elements to panel
        orderIdPanel.add(orderIdSearchLabel);
        orderIdPanel.add(orderIdSearchField);

        // panel for status selection
        // flowlayout to justify components to right
        JPanel orderStatusPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JLabel orderStatusLabel = new JLabel("Status:");

        // update statusLabels as content for combobox selections
        getStatusEnums();
        // setup combobox
        orderStatusBox = new JComboBox(statusLabels);
        orderStatusBox.setEditable(false);
        orderStatusBox.setPreferredSize(new Dimension(75, 20));
        // action handling for combobox
        orderStatusBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                // all = get all orders
                if (cb.getSelectedItem().equals("ALL")) {
                    model.getOrders();
                    LOGGER.info("client - Manager > call getOrders()");
                    // otherwise get orders with specified status
                } else {
                    model.getOrdersByStatus(cb.getSelectedItem().toString());
                    LOGGER.info("client - Manager > call getOrdersByStatus(" + cb.getSelectedItem() + ")");
                }
            }
        });
        // add elements to panel
        orderStatusPanel.add(orderStatusLabel);
        orderStatusPanel.add(orderStatusBox);

        // add panels to one panel
        ordersSelectionPanel.add(orderIdPanel);
        ordersSelectionPanel.add(orderStatusPanel);

        return ordersSelectionPanel;
    }

    /**
     * Erzeugt Panel welches eine Tabelle mit den Aufträgen beinhaltet
     *
     * @return Das Panel mit Aufragstabelle
     */
    private JComponent createOrdersTable() {
        JPanel ordersTablePanel = new JPanel();

        // use scrollpane to see labels and scrollbars of table
        JScrollPane scrollpane = new JScrollPane(table);
        // size of scrollpane
        scrollpane.setPreferredSize(new Dimension(400, 350));

        // set up column width
        TableColumn column = null;
        for (int i = 0; i < 5; i++) {
            column = table.getColumnModel().getColumn(i);
            if (i == 0) {        // ID
                column.setWidth(20);
                column.setPreferredWidth(20);
            } else if (i == 1 || i == 2) {
                column.setWidth(30);
                column.setPreferredWidth(30);
            } else if (i == 3) {
                column.setWidth(200);
                column.setPreferredWidth(200);
            } else {
                column.setWidth(30);
                column.setPreferredWidth(30);
            }
        }

        ordersTablePanel.add(scrollpane);

        return ordersTablePanel;
    }

    /**
     * Erzeugt eine Menüleiste für das Hauptframe
     *
     * @return Die Menüleiste
     */
    private JMenuBar createMenuToolbar() {
        JMenuBar menuBar = new JMenuBar();

        menuBar.add(m_file);
        m_file.add(mi_quit);

        menuBar.add(m_tasks);
        m_tasks.add(mi_collectOrders);

        menuBar.add(m_info);
        m_info.add(mi_info);

        addMenuItemsToListener();

        return menuBar;
    }

    /**
     * Befüllt das String-Array statusLabels mit allen verfügbaren Zuständen, mit Ausnahme von 'RESERVED'
     */
    private void getStatusEnums() {
        // first choise must be all orders
        statusList.add(new String("ALL"));
        // saves all values of Status to statusList
        for (Status s : Status.values()) {
            // ignore "RESERVED" status
            if (!(s.toString().equals("RESERVED")))
                statusList.add(s.toString());
        }
        statusLabels = statusList.toArray(statusLabels);
    }

    /**
     * Fragt in bestimmten Intervallen bestimmte Methoden ab
     */
    private void intervallCall() {
        try {
            while (running) {
                // all = get all orders
                synchronized (this) {
                    // wenn Auftragsstatus ausgewählt ist, dann werden diese Aufträge beim intervall "refresht"
                    if (orderStatusBox.getSelectedItem().equals("ALL")) {
                        model.getOrders();
                        LOGGER.info("client - Manager > call getOrders()");
                        // otherwise get orders with specified status
                    } else {
                        model.getOrdersByStatus(orderStatusBox.getSelectedItem().toString());
                        LOGGER.info("client - Manager > call getOrdersByStatus(" + orderStatusBox.getSelectedItem() + ")");
                    }

                    // wenn jedoch eine Auftragsnummer eingetragen ist, wird nur dieser Auftrag angezeigt
                    if (!orderIdSearchField.getText().isEmpty()) {
                        model.getOrderById(Integer.parseInt(orderIdSearchField.getText()));
                        LOGGER.info("client - Manager > call getOrderById(" + orderIdSearchField.getText() + ")");
                        LOGGER.info("orderId changed to " + orderIdSearchField.getText());
                    }

                    positionsPanel.getCargoPositions();
                    positionsPanel.getForkliftPosition();
                }

                // refresh every 2sec
                LOGGER.info("system > sleeped...");
                Thread.sleep(2000);
            }
        } catch (InterruptedException e) {
            // create error dialog
            new MessageDialogs(1, "Fehler beim automatischen refresh der Tabelle und Positionsgrid");
            LOGGER.severe(e.getMessage());
        }
    }

    /**
     * Fügt Auswahlfelder und -buttons dem Mouselistener hinzu, damit diese beim Mausklick ausgewertet werden können
     */
    private void addMenuItemsToListener() {
        MouseListener ml = new MouseListener();

        mi_info.addMouseListener(ml);
        mi_quit.addMouseListener(ml);
        mi_collectOrders.addMouseListener(ml);
        buttonCollectOrders.addMouseListener(ml);
    }

    /**
     * Innere Klasse zur Verarbeitung der MouseListeners / MouseEvents
     */
    class MouseListener extends MouseAdapter {
        public void mouseReleased(MouseEvent me) {
            if (me.getSource().equals(mi_quit)) {
                System.exit(0);
            }
            if (me.getSource().equals(mi_collectOrders) || me.getSource().equals(buttonCollectOrders)) {
                new CollectOrders(575, 250, "Neue Auftr\u00e4ge erfassen");
            }
            if (me.getSource().equals(mi_info)) {
                new InfoDialog(280, 280);
            }
        }
    }
}

package forklift.client.view;

import forklift.client.model.OrdersModelImpl;
import forklift.client.model.StagingOrdersModel;
import forklift.common.Order;
import forklift.common.Position;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: Alexander Steinbrecher
 * Date: 5/9/11
 * Time: 9:44 PM
 */

/**
 * Diese Klasse dient zur Erzeugung des Fensters zur Erfassung von Aufträgen
 *
 * @author Alexander Steinbrecher
 */
public class CollectOrders {
    private final static Logger LOGGER = Logger.getLogger(CollectOrders.class.getName());

    // mainFrame
    private JFrame mainFrame = new JFrame();

    // Table
    private StagingOrdersModel model = new StagingOrdersModel(); //new StagingOrdersModel();
    private JTable table = new JTable(model);

    JTextField idField, timeField;
    JComboBox comboBoxSourcePos, comboBoxDestinationPos;

    /**
     * Konstruktor für den Dialog zum Erfassen neuer Aufträge
     *
     * @param width Die Breite des Frame
     * @param height Die Höhe des Frame
     * @param titleWindow Titel des Frame
     */
    public CollectOrders(int width, int height, String titleWindow) {
        mainFrame.setTitle(titleWindow);
        mainFrame.setSize(width, height);

        // center window-position
        mainFrame.setLocationRelativeTo(null);

        // ui methods
        mainFrame.add(createMainPanel());

        mainFrame.setVisible(true);
    }

    /**
     * Erzeugt das Hauptpanel für das Fenster
     *
     * @return Das Hauptpanel
     */
    private JComponent createMainPanel() {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.LINE_AXIS));

        // add content to panel
        mainPanel.add(createNewOrderPanel());
        mainPanel.add(createStagingOrdersPanel());

        return mainPanel;
    }

    /**
     * Erzeugt das äußere Panel für Auftragsformular
     *
     * @return Das äußere Auftragsformularpanel
     */
    private JComponent createNewOrderPanel() {
        JPanel newOrderPanel = new JPanel();
        newOrderPanel.setBorder(BorderFactory.createTitledBorder("Auftrag"));

        // call method to include form
        newOrderPanel.add(createNewOrderForm());

        return newOrderPanel;
    }

    /**
     * Erzeugt das Auftragsformular selbst
     *
     * @return Das Auftragsformularpanel
     */
    private JComponent createNewOrderForm() {
        // idea from http://www.java2s.com/Code/Java/Swing-JFC/TextInputDemo.htm
        JPanel newOrderForm = new JPanel(new SpringLayout());

        String[] labelString = {"Quell-Position: ", "Ziel-Position: "};

        // save amount of labelString for loop
        int labelLength = labelString.length;

        JLabel[] labels = new JLabel[labelString.length];
        JComponent[] fields = new JComponent[labelString.length];
        int fieldCounter = 0;

        // set content for combobox "source-position"
        comboBoxSourcePos = new JComboBox(PositionsPanel.getValidPositions());
        fields[fieldCounter++] = comboBoxSourcePos;

        // set content for combobox "destination-position"
        comboBoxDestinationPos = new JComboBox(PositionsPanel.getValidPositions());
        fields[fieldCounter++] = comboBoxDestinationPos;

        for (int i = 0; i < labelString.length; i++) {
            labels[i] = new JLabel(labelString[i], JLabel.TRAILING);
            labels[i].setLabelFor(fields[i]);
            newOrderForm.add(labels[i]);
            newOrderForm.add(fields[i]);
        }

        // generate and include buttons to panel
        JButton buttonCancel = new JButton("Abbrechen");
        JButton buttonAdd = new JButton("Hinzuf\u00fcgen");
        newOrderForm.add(buttonCancel);
        newOrderForm.add(buttonAdd);

        // close window on click on button "cancel"
		buttonCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				mainFrame.setVisible(false);
			}
		});

        // close window on click on button "add"
		buttonAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
                // get OrderId
                int currentOrderId = Integer.parseInt(model.getOrderId());
                // add Order to stagingTable
                model.addStagingOrder(
                        new Order(
                            currentOrderId,
                            Position.valueOf(comboBoxSourcePos.getSelectedItem().toString()),
                            Position.valueOf(comboBoxDestinationPos.getSelectedItem().toString())
                        )
                );

                LOGGER.info("client - CollectOrders  > add Order(" + currentOrderId + ","
                        + comboBoxSourcePos.getSelectedItem() + ","
                        + comboBoxDestinationPos.getSelectedItem() + ") to stagingOrders");
            }
		});

        // to include buttons also to the CompactGrid, there must increased labelLength
        labelLength++;

        // panel-layout with class SprintUtilities
        //    info: http://download.oracle.com/javase/tutorial/uiswing/layout/spring.html
        // makeCompactGrid(panel, amount of labelString/rows, cols (label+textfield), init-x, init-y, x-pad, y-pad)
        SpringUtilities.makeCompactGrid(newOrderForm, labelLength, 2, 6, 6, 6, 6);

        newOrderForm.setOpaque(true);  //content panes must be opaque

        return newOrderForm;
    }

    /**
     * Erzeugt das äußere Panel für temporäre Aufträge
     *
     * @return Das äußere Panel für temporäre Aufträge
     */
    private JComponent createStagingOrdersPanel() {
        JPanel stagingOrders = new JPanel();
        stagingOrders.setBorder(BorderFactory.createTitledBorder("Auftr\u00e4ge"));

        // call methods to include elements
        stagingOrders.add(createStagingOrdersTable(), BorderLayout.WEST);
        stagingOrders.add(createStagingOrdersArrowButtons(), BorderLayout.EAST);
        stagingOrders.add(createStagingOrdersButtons(), BorderLayout.PAGE_END);

        return stagingOrders;
    }

    /**
     * Erzeugt eine Tabelle für temporäre Aufträge
     *
     * @return Ein Panel welches die Tabelle der temporären Aufträge enthält
     */
    private JComponent createStagingOrdersTable() {
        JScrollPane scrollpane = new JScrollPane(table);
        scrollpane.setPreferredSize(new Dimension(250, 150));

        TableColumn column = null;
        for (int i = 0; i < 3; i++) {
            column = table.getColumnModel().getColumn(i);
            if (i == 0) {        // ID
                column.setPreferredWidth(30);
            } else if (i == 3) { // Time
                column.setPreferredWidth(70);
            } else {
                column.setPreferredWidth(50);
            }
        }

        return scrollpane;
    }

    /**
     * Erzeugt Pfeil-Buttons für das Table
     *
     * @return Ein Panel welches die Pfeile enthält
     */
    private JComponent createStagingOrdersArrowButtons() {
        JPanel arrowButtonPanel = new JPanel(new GridLayout(2,1));

        JButton arrowUp = new JButton("\u2191");
        JButton arrowDown = new JButton("\u2193");

        arrowButtonPanel.add(arrowUp);
        arrowButtonPanel.add(arrowDown);

        // arrow up
        arrowUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
                // check if row is selected
                if (table.getSelectedRow() < 0)
                    new MessageDialogs(1, "W\u00e4hlen Sie eine Order aus!");
                else {
                    if (table.getSelectedRow() == 0) {
                        new MessageDialogs(1, "Order steht bereits an der ersten Position.");
                    } else {
                        model.moveUp(table.getSelectedRow());
                    }
                }
			}
		});

        // arrow down
        arrowDown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
                // check if row is selected
                if (table.getSelectedRow() < 0)
                    new MessageDialogs(1, "W\u00e4hlen Sie eine Order aus!");
                else {
                    if (table.getSelectedRow() == model.getStagingOrders().size()-1) {
                        new MessageDialogs(1, "Order steht bereits an der letzten Position.");
                    } else {
                        model.moveDown(table.getSelectedRow());
                    }
                }
			}
		});

        return arrowButtonPanel;
    }

    /**
     * Erzeugt Buttons für das Table
     *
     * @return Ein Panel welches die Buttons enthält
     */
    private JComponent createStagingOrdersButtons() {
        JPanel buttonPanel = new JPanel();

        JButton buttonRemoveStagingOrders = new JButton("L\u00f6schen");
        buttonPanel.add(buttonRemoveStagingOrders, BorderLayout.WEST);

        JButton buttonSaveStagingOrders = new JButton("Speichern");
        buttonPanel.add(buttonSaveStagingOrders, BorderLayout.WEST);

        // remove-button action
        buttonRemoveStagingOrders.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int selectedRow = -1;
                selectedRow = table.getSelectedRow();

                // proove if there is a row selected
                if (selectedRow != -1) {
                    // remove row when is selected
                    model.removeStagingOrder(selectedRow);

                    // debug
                    LOGGER.info("client - CollectOrders > call to " +
                            "remove stagingOrder (selectedRow=" + selectedRow + ")");
                } else if (selectedRow == -1) {
                    new MessageDialogs(1, "Es wurde keine Order ausgew\u00e4hlt.\n" +
                        "Bitte w\u00e4hlen Sie eine Order aus!");

                    // debug
                    LOGGER.info("client - CollectOrders > no stagingOrder selected") ;
                }
            }
        });

        // save and send the stagingOrders to server
        // close frame and removes stagingOrders ArrayList
		buttonSaveStagingOrders.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
                // send orders to server
                model.queueOrders();

                // remove stagingOrders
                model.clearStagingOrders();

                // refresh orders table in Manager
                OrdersModelImpl.getInstance().getOrdersModel().getOrders();

                // frame close
                mainFrame.setVisible(false);

                // info message when successful
                new MessageDialogs(2, "Auftragsliste wurde an den Server gesendet.");
			}
		});

        return buttonPanel;
    }
}

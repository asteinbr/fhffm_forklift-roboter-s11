package forklift.client.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by IntelliJ IDEA.
 * User: Alexander Steinbrecher
 * Date: 5/24/11
 * Time: 11:04 PM
 */

/**
 * Diese Klasse erzeugt das Informationsfenster
 *
 * @author Alexander Steinbrecher
 */
public class InfoDialog extends JFrame {

    String applicationName = "Forklift S11 - Manager";
    String applicationVersion = "m3";
    String applicatoinDescription = "A tool to manage the 'Forklift S11' robot";
    String applicationCopyright = "\u00a9 2011 D. Klein, J. M\u00e4usezahl, D. Ro\u00df, A. Steinbrecher";
    String roboterCopyright = "Roboter: J. M\u00e4usezahl";
    String backendCopyright = "Backend: D. Klein";
    String frontendCopyright = "Frontend: A. Steinbrecher";
    String docuCopyright = "Dokumentation: D. Ro\u00df";

    public InfoDialog(int x, int y) {

		this.setTitle(applicationName);
		this.setSize(x, y);
		// zentrale Positionierung des Fensters
		this.setLocationRelativeTo(null);
		this.setVisible(true);

		// Deklaration Labels und Positionierung
		JLabel labelApplicationName = new JLabel();
			labelApplicationName.setFont(new Font("",Font.BOLD,18));
			labelApplicationName.setText(applicationName);
			labelApplicationName.setAlignmentX(CENTER_ALIGNMENT);
		JLabel labelApplicationVersion = new JLabel();
			labelApplicationVersion.setText("v. " + applicationVersion);
			labelApplicationVersion.setAlignmentX(CENTER_ALIGNMENT);
		JLabel labelApplicationDescription = new JLabel();
			labelApplicationDescription.setText(applicatoinDescription);
			labelApplicationDescription.setAlignmentX(CENTER_ALIGNMENT);
		JLabel labelApplicationCopyright = new JLabel();
			labelApplicationCopyright.setFont(new Font("",Font.ITALIC,10));
			labelApplicationCopyright.setText(applicationCopyright);
			labelApplicationCopyright.setAlignmentX(CENTER_ALIGNMENT);

        // Copyrights
        JLabel labelRoboterCopyright = new JLabel();
            labelRoboterCopyright.setFont(new Font("",Font.ITALIC,10));
            labelRoboterCopyright.setText(roboterCopyright);
            labelRoboterCopyright.setAlignmentX(CENTER_ALIGNMENT);
        JLabel labelBackendCopyright = new JLabel();
            labelBackendCopyright.setFont(new Font("",Font.ITALIC,10));
            labelBackendCopyright.setText(backendCopyright);
            labelBackendCopyright.setAlignmentX(CENTER_ALIGNMENT);
        JLabel labelFrontendCopyright = new JLabel();
            labelFrontendCopyright.setFont(new Font("",Font.ITALIC,10));
            labelFrontendCopyright.setText(frontendCopyright);
            labelFrontendCopyright.setAlignmentX(CENTER_ALIGNMENT);
        JLabel labelDocuCopyright = new JLabel();
            labelDocuCopyright.setFont(new Font("",Font.ITALIC,10));
            labelDocuCopyright.setText(docuCopyright);
            labelDocuCopyright.setAlignmentX(CENTER_ALIGNMENT);

		JLabel labelSpaceDummy1 = new JLabel(" ");
		JLabel labelSpaceDummy2 = new JLabel(" ");
		JLabel labelSpaceDummy3 = new JLabel(" ");
		JLabel labelSpaceDummy4 = new JLabel(" \n");
        JLabel labelSpaceDummy5 = new JLabel(" \n");
		JButton buttonClose = new JButton();
			buttonClose.setText("Schlie\u00dfen");
			buttonClose.setAlignmentX(CENTER_ALIGNMENT);

		Container contentPane = getContentPane();
		this.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

		contentPane.add(labelSpaceDummy1);
        contentPane.add(labelApplicationName);
		contentPane.add(labelApplicationVersion);
		contentPane.add(labelSpaceDummy2);
		contentPane.add(labelApplicationDescription);
		contentPane.add(labelSpaceDummy3);
		contentPane.add(labelApplicationCopyright);
		contentPane.add(labelSpaceDummy4);
        contentPane.add(labelRoboterCopyright);
        contentPane.add(labelBackendCopyright);
        contentPane.add(labelFrontendCopyright);
        contentPane.add(labelDocuCopyright);
        contentPane.add(labelSpaceDummy5);
		contentPane.add(buttonClose);

		// Action Button "Close / Schließen"
		buttonClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				setVisible(false);
				dispose();
			}
		});
	}
}